# Interaction plasticity and the expansion of Cys2His2 zinc finger domains. 

## Introduction


The Cys 2 His 2 zinc finger is the most common DNA-binding domain expanding from fungi
to humans. A catalyst for this expansion is an arms race to repress transposable
elements; however, mechanisms that explain the evolution of this domain’s specificity
are limited. Despite the prevalence of this domain, models of its DNA binding specificity
remain error-prone likely due to a simplistic understanding of how adjacent fingers
influence one another. Here we use a synthetic approach to exhaustively investigate
one of the dominant influences on nearby finger function, geometry. By screening over
28 billion protein-DNA interactions in various geometric contexts, we find the plasticity of
the most common geometry found in human transcription factors enables more functional amino acid combinations across all targets. Further, residues that define this geometry are enriched in human, suggesting their selection as the domain has expanded, and factors that use these residues can more readily transition their specificity as evolution demands. Finally, these results demonstrate that this synthetic
screen of one of the influences on zinc finger function can provide a model as accurate
as any prior model while providing mechanistic insight that assisted the domain’s
expansion.
