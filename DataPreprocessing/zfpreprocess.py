import sys
import glob
import pandas as pd
import numpy as np

from ngskit import analysis
from ngskit.utils import fastq_tools
from ngskit.utils import dna



t = sys.argv[1]
#t = 'TTT'
mode = sys.argv[2]
#mode = 'MODE6'
folder = sys.argv[3]
#folders = '/home/ccorbi/Work/Beagle/SEQ_2018-01-05-ZF/'

fn = '{}*{}*R1*fastq.gz'.format(mode, t)
#M4_TCA_S130_L002_R2_001.fastq.gz

folders = folder + fn

fastqs = glob.glob(folders)

tdata = list()
print(fastqs, folders) # folders.format(mode, t))
for fq in fastqs:
    a = fastq_tools.read_fastq(fq)
    tdata.extend(a)
    
df = pd.DataFrame(a, columns=['Seq', 'avg_phred','phred'])
df.head()

df['Ns'] = df['Seq'].str.contains('N')
df['STOP'] = df['Seq'].apply(dna.code_4_any)
# filter by previous criteria and translate to AA
dfc = df[(df['STOP']==False)&(df['Ns']==False)&(df['avg_phred']>=30)]

df_groupsdna =dfc.groupby('Seq',as_index=False).agg(len)
del df_groupsdna['phred']
del df_groupsdna['Ns']
del df_groupsdna['STOP']
df_groupsdna.rename(columns={'avg_phred':'Reads'}, inplace=True)



df_groupsdna['aa'] = df_groupsdna['Seq'].apply(dna.translate2aa)

df_groups_aa =df_groupsdna.groupby('aa')
# Calc Shannon Entropy
parsed_aa = list()
for aa, df_grp in df_groups_aa:
    # if the group only have one member Check
    # if is variants = 1, otherwise, 0 Entropy
    if df_grp.shape[0] == 1:
        N = dna.possible_encondings(aa)
        if N == 1:
            # Seq, Reads, Entropy
            parsed_aa.append([aa, df_grp['Reads'].sum(), 1])
        else:
            parsed_aa.append([aa, df_grp['Reads'].sum(), 0])
    else:
        N = dna.possible_encondings(aa)
        df_grp['prob'] = df_grp['Reads'] / df_grp['Reads'].sum(skipna=True)
        df_grp['eprob'] = df_grp['prob'] * (np.log2( df_grp['prob']))

        shannon_entropy = -1 * ( (1.0/(np.log2(N))) *  (df_grp['eprob'].sum(skipna=True)))

        # Seq, Reads, Entropy
        parsed_aa.append([aa, df_grp['Reads'].sum(),df_grp.shape[0], shannon_entropy])
results =  pd.DataFrame(parsed_aa, columns=['Seq', 'Reads','Var' ,'E'])

ent = results[results['E']>0]

ent.to_csv('{}_{}_preprocess.csv'.format(mode,t), index=False)