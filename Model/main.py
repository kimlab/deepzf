import argparse

from data import *
from model import *


def get_options():
    """Get arguments from command line.
    Parameters
    ----------
    Returns
    -------
    """
    parser = argparse.ArgumentParser(description="""DeepZF
    """)

    # model architecture
    parser.add_argument('-m', '--model_cfg', dest="configfile", help='json file with network architecture and model configuration',
                         required=True)

    # Training parameters
    parser.add_argument('--epochs', action="store",
                        dest="epochs", default=250, type=int,
                        help='Number of epochs')

    parser.add_argument('--batch_size', action="store",
                        dest="batch", default=512, type=int,
                        help='Batch size')

    parser.add_argument('-v', action="store",
                        dest="verb", default=0, type=int,
                        help='verbose level')        
   

    # Training Dataset Options
    parser.add_argument('--entropy_threshold', action="store",
                        dest="entropy", default='0.03', type=str,
                        help='Entropy Threshold to filter out training sequences')

    parser.add_argument('--reads_threshold', action="store",
                        dest="reads", default='5', type=str,
                        help='Reads threshold to filter out training sequences')

    parser.add_argument('--data_augmentation', help='How many new BP are graft, default no data agumentation', dest='data_augmentation',
                        default=0, type=int, action='store')

    parser.add_argument('--modes_list', help='Modes to be use in the training, comma o dash separated, default All', dest='modes_list', 
                        default='RA,RS,MODE2,MODE3,MODE4,MODE5,MODE6', action='store')

    options = parser.parse_args()

    return options


# MAIN
def main():


    opts = get_options()


    modes = opts.modes_list.split(',')
    modes_label = opts.modes_list.replace(',','-')

    #stringent_level = sys.argv[3]
    datap = DataPrep(folder_data='/home/kimlab2/ccorbi/MARCUS_ZF/preprocess_complete/', 
                    read_cutoff=opts.reads, 
                    entropy_cutoff=opts.entropy, 
                    reads_fn='normreadsNL',
                    load_modes=modes)
    #datap = DataPrep(read_cutoff=15, entropy_cutoff=0.04)
    callback = [EarlyStopping(patience=10),
                    ReduceLROnPlateau(patience=3, verbose=1),]



    # load dataset
    datap.load()
    f2X,f2y = load_F2data()
    f3X,f3y = load_F3data()
    timx, timy = load_timdata()

    output_id = '{}_{}_{}_{}_{}'.format(opts.configfile, opts.entropy, opts.reads, modes_label, opts.data_augmentation  )


    log = open(f'LOG_{output_id}_run.out','w')

    # yloss
    myloss = {"MSE":'mean_squared_error',
            'KL': 'categorical_crossentropy',
            'CORR': correlation_coefficient_loss,
            }


    configuration = json.load(open('{}.json'.format(opts.configfile),'r'))

    l = myloss[configuration['loss_on']]
    if 'finalAct' in configuration:

        fa = configuration['finalAct']

    else:
        fa = 'softmax'



    gsd_results, gsd_results_mse = list(), list()
    folds = 1
    f2_mse, f3_mse, vald_mse, hamed_mse = list(), list(), list(), list()

    # 10-fold cross validations
    for X,y, test_XM, test_yM in datap.kf(splits=10):
        archy = Mymodel_generator(aminoacids=21, seq_len=8, outshape=12,
                                loss=l,lr=configuration['lr'],
                                architecture= configuration['archy'],
                                optimizer=configuration['optim'], finalAct=fa)

        H = archy.model.fit(X, y, epochs=opts.epochs,
                                        verbose=opts.verb,
                                        batch_size=opts.batch,
                                        shuffle=True,
                                        validation_data=(test_XM,test_yM),
                                        callbacks=callback)

        #archy.model.save_weights('model_weights_{}.h5'.format(folds))

        plot_train(H,output_name=f'{folds}_{output_id}')

        e = archy.model.evaluate(timx,timy)
        print(e)
        print('Validation_Tim_fold_{} {} {}'.format(folds,e[1],e[2]), file=log)

        e = archy.model.evaluate(f2X,f2y)
        print(e)
        print('Validation_F2_fold_{} {} {}'.format(folds,e[1],e[2]), file=log)


        e = archy.model.evaluate(f3X,f3y)
        print(e)
        print('Validation_F3_fold_{} {} {}'.format(folds,e[1],e[2]), file=log)
            ##gmodel.plot(output_name=mode+'_'+configfile)



        r2 = evaluate_GSD(archy.model)
        # extract corr data
        flat_r2 = list(r2.values())
        print('GSDStromo_fold_{}  {}  {}  {}  {}  {} {}'.format(folds, np.nanpercentile(flat_r2, 25), np.nanmean(flat_r2), np.nanmedian(flat_r2), np.nanpercentile(flat_r2, 75),np.nanmin(flat_r2), np.nanmax(flat_r2)), file=log)
        gsd_results.append(flat_r2)
        vald_mse.append(mse_pool(test_XM,test_yM, archy.model))
        hamed_mse.append(mse_pool(timx,timy, archy.model))
        f2_mse.append(mse_pool(f2X,f2y, archy.model))
        f3_mse.append(mse_pool(f3X,f3y, archy.model))

        r3 = evaluate_GSD_MSE(archy.model)
        # extract corr data
        flat_r3 = list(r3.values())
        print('GSDStromo_MSE_{}  {}  '.format(folds,np.nanmean(flat_r3) ), file=log)
        gsd_results_mse.append(flat_r3)

        folds +=1

    log.close()


    rfdata = pickle.load(open('../Data/RF_performance_GSStromo.pkl','rb'))
    gsd_results.append(rfdata)
    plot_correlation(gsd_results, output_name=f'GSD_{folds}_{output_id}')

    
    rfdata = pickle.load(open('../Data/RF_performance_GSStromo_MSE.pkl','rb'))
    gsd_results_mse.append(rfdata)
    plot_correlation(gsd_results_mse, output_name=f'GSDMSE_{folds}_{output_id}')
    

    # HamedRF_benchMArcus_MSEgeneral.pkl
    i = pickle.load(open('HamedRF_benchMArcus_MSEgeneral.pkl','rb'))
    vald_mse.append(i)
    plot_correlation(vald_mse, output_name=f'EVAL_{folds}_{output_id}',  swarm=False)

    # HamedRF_benchTim_MSEgeneral.pkl
    i = pickle.load(open('HamedRF_benchTim_MSEgeneral.pkl','rb'))
    hamed_mse.append(i)
    plot_correlation(hamed_mse, output_name=f'TH_{folds}_{output_id}', swarm=False)
    
    # HamedRF_benchNoyesF2_MSEgeneral.pkl
    i = pickle.load(open('HamedRF_benchNoyesF2_MSEgeneral.pkl','rb'))
    f2_mse.append(i)
    plot_correlation(f2_mse, output_name=f'F2_{folds}_{output_id}', swarm=False)

    # HamedRF_benchNoyesF3_MSEgeneral.pkl    
    i = pickle.load(open('HamedRF_benchNoyesF3_MSEgeneral.pkl','rb'))
    f3_mse.append(i)
    plot_correlation(f3_mse, output_name=f'F3_{folds}_{output_id}', swarm=False)

#plt.boxplot(cnn_toplot_mse_triplet)




if __name__ == "__main__":
    main()