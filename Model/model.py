import sys
import json
import os
import pickle
import random
import argparse
import math
import re
from collections import defaultdict
import numpy as np
import pandas as pd
import json
import matplotlib
matplotlib.use('agg')


from scipy.stats import pearsonr
from scipy.stats import entropy
from sklearn.metrics import mean_squared_error


import matplotlib.pyplot as plt

import keras.backend as K
from keras import backend as K
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.initializers import RandomNormal
from keras.layers import Dense, LSTM, GRU, Dropout, Embedding
from keras.models import Sequential, load_model, Model
from keras.layers import Conv2D, Conv2DTranspose, UpSampling2D, MaxPooling2D, Convolution2D, Convolution1D
from keras.layers import Conv1D,  UpSampling1D, MaxPooling1D, LSTM, Dropout, TimeDistributed
from keras.layers import LeakyReLU, Dropout, GlobalMaxPooling1D, Input, Concatenate
from keras.layers import Dense, Dropout, Flatten, Activation, BatchNormalization, regularizers, Reshape
from keras.layers import InputLayer,  regularizers


from keras.optimizers import Adam, SGD, RMSprop
from keras.regularizers import l2

import tensorflow as tf

from sklearn.utils import shuffle
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler

from scipy.spatial import distance
from scipy.stats import pearsonr

from sklearn.model_selection import KFold

from scipy.stats import pearsonr
from scipy.stats import spearmanr

from keras.callbacks import EarlyStopping, ReduceLROnPlateau, CSVLogger, ModelCheckpoint

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)



def _translate_optimizer(token, lr):

    if token[0] == 'SGD':
        return  SGD(lr=lr,**token[1])

    if token[0] == 'ADAM':
        return  Adam(lr=lr,**token[1])

    if token[0] == 'RMSprop':
        return  RMSprop(lr=lr,**token[1])


def _translate_2keras(item):

    if item[0] == 'Conv1D':
        return Conv1D(**item[1])

    if item[0] == 'Conv2D':
        return Conv2D(**item[1] )

    if item[0] == 'Maxpool2':

        return MaxPooling2D()

    if item[0] == 'Maxpool':

        return MaxPooling1D()

    if item[0] == 'Dense':
        return Dense(**item[1])

    if item[0] == 'Dropout':
        return Dropout(**item[1])

    if item[0] == 'Flatten':
        return Flatten()

    if item[0] == 'LSTM':
        return LSTM(**item[1])

    if item[0] == 'Batchnorm':
        return BatchNormalization()

    if item[0] == 'L1':
        return regularizers.l1(**item[1])
    if item[0] == 'L2':
        return regularizers.l2(**item[1])



def correlation_coefficient_loss(y_true, y_pred):
    x = y_true
    y = y_pred
    mx = K.mean(x)
    my = K.mean(y)
    xm, ym = x-mx, y-my
    r_num = K.sum(tf.multiply(xm,ym))
    r_den = K.sqrt(tf.multiply(K.sum(K.square(xm)), K.sum(K.square(ym))))
    r = r_num / r_den

    r = K.maximum(K.minimum(r, 1.0), -1.0)
    return 1 - K.square(r)




class Mymodel_generator(object):
    """
    Class containing the LSTM model to learn sequential data
    """

    def __init__(self, aminoacids, seq_len, outshape,
                lr=0.001,  loss='mean_squared_error',
                architecture = [ ('Conv1D',{'nb_filter':32 ,
                                    'filter_length':6,
                                    'activation':'relu',
                                    'padding':'same'}), # number len activation padding
                     ('maxp',),
                     ('flatten',)], optimizer = [('ADAM',{'beta_1':0.9, 'beta_2':0.999, 'epsilon':1e-08, 'decay':0.0})],
                 finalAct='softmax',
                 mode='MODE5',
                    seed=42):
        """ Initialize the model
        :param n_vocab: {int} length of vocabulary
        :param outshape: {int} output dimensionality of the model
        :param session_name: {str} custom name for the current session. Will create directory with this name to save
        results / logs to.
        :param n_units: {int} number of LSTM units per layer
        :param batch: {int} batch size
        :param layers: {int} number of layers in the network
        :param loss: {str} applied loss function, choose from available keras loss functions
        :param lr: {float} learning rate to use with Adam optimizer
        :param dropoutfract: {float} fraction of dropout to add to each layer. Layer1 gets 1 * value, Layer2 2 *
        value and so on.
        :param l2_reg: {float} l2 regularization for kernel
        :param seed {int} random seed used to initialize weights
        """
        self.seq_len = seq_len
        self.aminoacids = aminoacids
        self.outshape = outshape
        self.optim = optimizer
        self.finalAct = finalAct
        self.loss = loss
        self.lr =lr
        self.arch = architecture
        self.seed = seed
        self.initialize_model(seed=self.seed)
        #self.load_training(mode)


    def initialize_model(self, seed=42):
        """ Method to initialize the model with all parameters saved in the attributes. This method is used during
        initialization of the class, as well as in cross-validation to reinitialize a fresh model for every fold.
        :param seed: {int} random seed to use for weight initialization
        :return: initialized model in ``self.model``
        """

        weight_init = RandomNormal(mean=0.0, stddev=0.05, seed=seed)  # weights randomly between -0.05 and 0.05

        inputs = Input(shape=(self.seq_len, self.aminoacids))

        x = _translate_2keras(self.arch[0])(inputs)
        for layer in self.arch[1:]:
            print(layer)
            x = _translate_2keras(layer)(x)

        tri1 = Dense(self.outshape,activation=self.finalAct, name='Triplet_1')(x)

        model = Model(inputs, tri1)

        #optimizer = Adam(lr=self.lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
        optimizer  = _translate_optimizer(self.optim, self.lr)
        model.compile(loss=self.loss, optimizer=optimizer,  metrics=["MSE", correlation_coefficient_loss])

        self.model =  model

