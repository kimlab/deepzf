import sys
import glob
import pandas as pd
import numpy as np
import re
import pickle

from ngskit.analysis import *
from sklearn.utils import shuffle


all_amino = ['_','C', 'V', 'T', 'F', 'Y', 'A', 'P', 'W', 'I', 'M', 'L', 'S', 'G', 'H', 'D', 'E', 'N', 'Q', 'R', 'K']
n_amino = len(all_amino)
aa_to_int = dict((c, i) for i, c in enumerate(all_amino))

int_to_aa = dict((c, i) for i, c in aa_to_int.items())



aa_one_hot = dict()
for a,i in aa_to_int.items():
    v = np.zeros(len(aa_to_int))
    v[i] = 1
    aa_one_hot[a] = v
    
triplets = list()
bases = ['A','C' ,'G', 'T']

for b1 in bases:
    for b2 in bases:
        for b3 in bases:
            triplets.append(b1+b2+b3)
            
n_bases = len(bases)

base_to_int = dict((c, i) for i, c in enumerate(bases))
int_to_base = dict((c, i) for i, c in base_to_int.items())



base_one_hot = dict()
for a,i in base_to_int.items():
    v = np.zeros(len(base_to_int))
    v[i] = 1
    base_one_hot[a] = v







def _tofreq(value, maxim):
    b = maxim / np.log(50)
    
    #a =   math.pow(10, value) /4
    a = np.exp(value / b)
    #print(a)
    return a + 0.00000001
    
def __resize(vectors):
    matrix = list()
    for i in range(0,len(vectors),4):
        matrix.append(vectors[i:i+4])
    m =  pd.DataFrame(matrix, index=list(range(3)),columns=('A','C','G','T')  ).T
    m=  m.T.as_matrix()
    return np.reshape(m,(12))


def timscore2freq(vectors):
    matrix = list()
    for i in range(0,len(vectors),4):
        matrix.append(vectors[i:i+4])
    m =  pd.DataFrame(matrix, index=list(range(3)),columns=( 'A','C' ,'G', 'T')  ).T
    max_range = max(m.max() - m.min())
    m=  m.apply(_tofreq, maxim=max_range, axis=1)
    m =  m / m.sum()
    m=  m.T.as_matrix()
    return np.reshape(m,(12))


def _itersplit_motif(zfinger):
    region = list()
    # Reg 0
    zfinger = 'X'+ zfinger
    re_motifs = [ '.{1,5}C.{11,15}H.{2,6}[HC]', '.{1,5}C.{11,15}H',]
    
    seq = re.findall(re_motifs[0],zfinger )
    if seq:
        seq = re.findall(re_motifs[1],seq[0])

    
    return seq



def extract_spec_res_tim(zfinger):
    parts = list()
    zregion = _itersplit_motif(zfinger)
    if len(zregion) !=0:
        for i in [-8,-7,-6,-5,-3,-2]:
            parts.append(zregion[0][i:i+1])
        return ''.join(parts)
    return None


def extract_spec_res_GSD(zfinger):
    parts = list()
    #zregion = itersplit_motif(zfinger)
    #print(zregion)
    if len(zfinger) !=0:
        for i in [-8,-7,-6,-5,-3,-2]:
            parts.append(zfinger[i:i+1])
        return ''.join(parts)
    return None



def extract_spec_res_marcus(zfinger):
    parts = list()
    #zregion = itersplit_motif(zfinger)
    if len(zfinger) !=0:
        for i in [8,9,10,11,13,14]:
            parts.append(zfinger[i:i+1])
        return ''.join(parts)
    return None




def marcusdf2pfm(Seq, df, ignore_if = None):
    temp = df[df['Seq']==Seq]
    buffer = list()
    #return temp
    for triplet in triplets.keys():
        if triplet in temp['Triplet'].values:
            amount = a['Reads'][a['Triplet']==triplet].values[0]
            if ignore_if:
                if ignore_if > amount:
                    continue
            
            for x in range(int(amount)):
                buffer.append(triplet)
                
    pf = get_ppm(buffer,pseudocounts=0, elements=bases)
    return pf

def onehot_seq(seq, kind = 'aa'):
    if kind == 'aa':
        one_hot = aa_one_hot
    else:
        one_hot = base_one_hot
        
    _one_hot_seq =  list()
    for s in seq:
        _one_hot_seq.append(one_hot[s])
    
    return np.asarray(_one_hot_seq)



def revertoseq(vec, kind='aa'):
    
    if kind == 'aa':
        int_to_char = int_to_aa
    else:
        int_to_char = int_to_base
    
    seq = list()
    for i in vec:
        c = list(i > 0)
        seq.append(int_to_char[c.index(True)])
    return ''.join(seq)

# Vecotirize
from ngskit.analysis  import *
# scale
norm_props = dict()
for prop, data in aa_property_tables.items():
    minprop = min(data.values())
    maxprop = max(data.values())
    c = maxprop - minprop
    norm = dict()
    for aa, value in data.items():
        norm[aa] = (value - minprop) / c
    norm_props[prop] = norm
    
    
def embeedings(seq):
    
    vec = np.zeros((len(seq),6))
    
    for idx,a in enumerate(seq):
        vec[idx][0] = norm_props['charge'].get(a, 0.0)
        vec[idx][1] = norm_props['polarity'].get(a, 0.0)
        vec[idx][2] = norm_props['hydrophobicity'].get(a, 0.0)
        vec[idx][3] = norm_props['volume'].get(a, 0.0)
        vec[idx][4] = norm_props['local_flexibility'].get(a, 0.0)
        vec[idx][5] = norm_props['pK'].get(a, 0.0)
    return vec

def get_decoder():
    decoder = dict()
    for a in norm_props['charge'].keys():
        indx = list()
        for p in ['charge','polarity', 'hydrophobicity', 'volume', 'local_flexibility','pK']:
            indx.append(norm_props[p].get(a, 0.0))
        decoder[set(indx)]= a
        
    return decoder


def encode_data(df):
    dataX_Tim = list()
    dataY_Tim = list()
    for idx, row in df.iterrows():

        seq_out = row['Seq'].replace('X','A')
        #reprocess = extract_motifs(seq_out)
        #seq_out = ''.join(reprocess)
        try:
            #zmotif = itersplit_motif(seq_out)
            seq_out = extract_spec_res_marcus(seq_out)
            seq_out = seq_out.replace('X','A')
        except:
            continue
        query = onehot_seq(seq_out, kind = 'aa')
        #while len(query) < 29:
        #    query.append(0)

        query = np.array(query)
        query = np.reshape(query,(6,21,1))
        #target = [to_one_hot[char] for char in seq_out]
        features = [ 'A1', 'C1', 'G1', 'T1', 'A2', 'C2', 'G2', 'T2', 'A3', 'C3', 'G3', 'T3'] #'A4', 'C4', 'G4', 'T4',]

        target = row[features]

        target = np.array(target)
        #target = __convertscore2freq(target)
        #dataX.append(np.reshape(result, (1, result.shape[0], result.shape[1])))
        #dataY.append(np.array(to_one_hot[seq_out]))
        dataX_Tim.append(query)
        dataY_Tim.append(target)
    
    X =  np.asarray(dataX_Tim)
    y = np.asarray(dataY_Tim)

    X, y = shuffle(X,y)
    
    return X, y



#s = "DICGRKFRSGSALWHHTKIHLRQKD"
# function to transform reads to pfm
# no pseudo counts allowed
def calc_ppm(s, ddf):
    df = ddf[ddf['Seq']==s]
    buffer = list()
    for t in triplets:
        if t in df['Triplet'].values:
            rep = df[(df['Triplet']==t)]['Reads'].values[0]
            for i in range(int(rep)):
                buffer.append(t)
    freqs = get_ppm(buffer,elements=bases,pseudocounts=0.0)
    return list(freqs.values.flatten(order='F'))

def calc_ppm_log_flat(s, ddf):
    df = ddf[ddf['Seq']==s]
    buffer = list()
    for t in triplets:
        if t in df['Triplet'].values:
            rep = df[(df['Triplet']==t)]['Reads'].values[0]
            rep = int(np.log10(rep)) 
            for i in range(int(rep)):
                buffer.append(t)
    freqs = get_ppm(buffer,elements=bases,pseudocounts=0.0)
    return list(freqs.values.flatten(order='F'))


def calc_ppm_flat(s, ddf):
    df = ddf[ddf['Seq']==s]
    buffer = list()
    for t in triplets:
        if t in df['Triplet'].values:
            rep = df[(df['Triplet']==t)]['Reads'].values[0]
            rep = 1
            for i in range(int(rep)):
                buffer.append(t)
    freqs = get_ppm(buffer,elements=bases,pseudocounts=0.0)
    return list(freqs.values.flatten(order='F'))


def calc_ppm_usingnormreads(s, ddf):
    df = ddf[ddf['Seq']==s]
    buffer = list()
    for t in triplets:
        if t in df['Triplet'].values:
            rep = df[(df['Triplet']==t)]['nReads'].values[0]
            #print(df)
            for i in range(int(rep)):
                buffer.append(t)
    try:
        freqs = get_ppm(buffer,elements=bases,pseudocounts=0.0)

    except Exception as e:
        print(rep)
        print('s')
        raise e

    return list(freqs.values.flatten(order='F'))

def loground(val):
    return round(np.log2(val))

# put all triplets togheter
collector = list()
m = sys.argv[1]

for t in  triplets:
    try:
        df = pd.read_csv(f'./{m}/{t}_amino_acid_union_cut.txt', names=['Seq','Reads'], delim_whitespace=True)
        df['Triplet'] = t
        collector.append(df)
    except:
        print('Something went wrong for {}'.format(t))
        pass


mergeddf = pd.concat(collector)
#TRIM

#FILTERS


FRQ = 'normreads'
if sys.argv[2]:
    if sys.argv[2].strip() == 'log':
        print('log reads')
        FRQ = 'logreads'
        calc_ppm_fn = calc_ppm_log_flat
    elif sys.argv[2].strip() == 'flat':
        print('flat reads')
        FRQ = 'flatreads'
        calc_ppm_fn = calc_ppm_flat
    elif sys.argv[2].strip() == 'normreads':
        print('normreads')
        FRQ = 'normreads'
        calc_ppm_fn = calc_ppm_usingnormreads

    else:
        print('reads')
        calc_ppm_fn = calc_ppm
else:
    calc_ppm_fn = calc_ppm_usingnormreads


mode_aa_map = {'F3':'_A','F2':'KS'} 
mode_aa = mode_aa_map[m]


# mandatory filters
# this norm is to  remove bias in deep of the seq, and reduce the weight of outliers
mergeddf['nReads'] =  np.log2(mergeddf['Reads']*1_000_000)

# remove mutants
mergeddf['MODE_AA'] = mode_aa

seq_poll = mergeddf['Seq'].unique()

# transform and generate a new dataframe 
# Seq  'A1', 'C1', 'G1', 'T1', 'A2', 'C2', 'G2', 'T2', 'A3', 'C3', 'G3', 'T3'
totals = list()
for s in seq_poll:
    ppm = calc_ppm_fn(s, mergeddf)
    totals.append([s]+ppm)
# Pandify 
fet= [ 'A1', 'C1', 'G1', 'T1', 'A2', 'C2', 'G2', 'T2', 'A3', 'C3', 'G3', 'T3']
na = ['Seq'] + fet
totals = pd.DataFrame(totals, columns=na)

arra = np.random.random(totals.shape[0])
train = arra <.8
test = arra>=.8


df = totals.copy()
df.to_csv('{}_totalds.csv'.format(m), index=False)
X, y = encode_data(df)
pickle.dump(X, open('{}_Xtotal.pkl'.format(m,),'wb'))
pickle.dump(y, open('{}_ytotal.pkl'.format(m,),'wb'))




#temp = mergeddf[mergeddf['Seq'].isin(df['Seq'])]
#temp.to_csv('{}_trainingset_classf_{}_{}_{}.csv'.format(m,FRQ, READS_CUTOFF,E_CUTOFF), index=False)
# for class
#temp = mergeddf[mergeddf['Seq'].isin(df['Seq'])]
mergeddf.to_csv('{}_trainingset_classf.csv'.format(m), index=False)




        