# DataPack

Datasets used on Interaction plasticity and the expansion of Cys2His2 zinc finger domains. 

- Raw_Hughes.2015.csv. Complete set (47K) of zinc fingers recollected in Hughes, 2015 paper (see below for full reference). 

- B1H_Hughes.2015.csv. B1H data from C2H2 zinc finger proteins greatly expand the human regulatory lexicon. Najafabadi HS, Mnaimneh S, Schmitges FW, Garton M, Lam KN, Yang A, Albu M, Weirauch MT, Radovani E, Kim PM, Greenblatt J, Frey BJ, Hughes TR. Nature Biotechnol. 2015 Feb 18.

- NAR_Noyes.2015. Data from A systematic survey of the Cys2His2 zinc finger DNA-binding landscape. Anton V. Persikov Joshua L. Wetzel Elizabeth F. Rowland Benjamin L. Oakes Denise J. Xu, Mona Singh Marcus B. Noyes. NAR 2015

- DMel_Wolfe.2013.csv Data from Global analysis of Drosophila Cys2-His2zinc finger proteins reveals a multitude of novel recognition motifs and binding determinants. Scot A. Wolfe et al.   Genome Research 2013 

- Uniprot_ZFC2H2.2019. All C2H2 zinc fingers in Uniprot. If the zinc finger is canonical, (matches the [Prosite definition](https://prosite.expasy.org/PDOC00028) ), the specificity resides of the helix and the boundary pairs where extracted. Otherwise, an X was assigned.
